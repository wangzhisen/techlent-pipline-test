type Args = number[] | number[][];
/**
 *
 * @param args :支持传入number多个参数，或者单个数组作为参数
 * @returns number
 * 计算数组或者多个参数的求和
 */
export function add(...args: Args) {
  const flattenArgs = args.flat(1);
  return flattenArgs.reduce(
    (total: number, current: number) => total + current,
    0
  );
}
