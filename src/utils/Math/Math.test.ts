import { add } from "./Math";

/**
 * Feat: 测试脚本，测试add函数的不同入参类型
 */

describe("函数 [add] 测试:", () => {
  test("传入多个number类型作为参数", () => {
    expect(add(1, 2, 3)).toBe(6);
  });

  test("传入单个number数组作为参数", () => {
    expect(add([10, 20, 30])).toBe(60);
  });
});
