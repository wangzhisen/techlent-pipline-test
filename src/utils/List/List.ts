/**
 * 实现简单数组的去重处理
 * @param list:T[]
 * @returns list:T[]
 */

export function uniqList<T>(list: T[]): T[] {
  const set = new Set([...list]);
  return Array.from(set);
}
