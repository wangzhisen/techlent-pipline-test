import { uniqList } from "./List";

/**
 * Feat: 测试脚本，测试uniqList函数的去重功能
 */

describe("函数 [uniqList] 测试:", () => {
  test("传入多个number类型作为参数", () => {
    expect(uniqList([1, 2, 3, 3])).toEqual([1, 2, 3]);
  });

  test("传入string数组作为参数", () => {
    expect(uniqList(["aaa", "bbb", "ccc", "bbb"])).toEqual([
      "aaa",
      "bbb",
      "ccc",
    ]);
  });
});
