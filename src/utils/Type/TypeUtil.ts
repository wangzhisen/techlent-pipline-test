export function isFn(target?: any): target is Function {
  return typeof target === "function";
}
