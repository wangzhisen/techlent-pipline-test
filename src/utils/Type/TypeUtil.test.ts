import { isFn } from "./TypeUtil";

/**
 * Feat: 测试脚本，测试add函数的不同入参类型
 */

describe("函数 [add] 测试:", () => {
  test("传入箭头函数作为参数", () => {
    expect(isFn(() => 10)).toBe(true);
  });

  test("传入function声明的函数作为参数", () => {
    expect(
      isFn(function () {
        return 20;
      })
    ).toBe(true);
  });

  test("传入以变量形式声明的函数作为参数", () => {
    const fn = function () {
      return 30;
    };
    expect(isFn(fn)).toBe(true);
  });

  test("传入自执行函数作为参数", () => {
    expect(isFn((function () {})())).toBe(false);
  });

  test("传入数组作为参数", () => {
    expect(isFn([10, 20, 30])).toBe(false);
  });

  test("传入布尔值作为参数", () => {
    expect(isFn(false)).toBe(false);
  });

  test("传入数字作为参数", () => {
    expect(isFn(10)).toBe(false);
  });

  test("传入对象作为参数", () => {
    expect(isFn({ a: 10 })).toBe(false);
  });
});
