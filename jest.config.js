/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  // 哪些文件需要用 ts-jest 执行
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  // Jest使用模式或模式来检测测试文件
  testRegex: "(/__test__/.*|(\\.|/)(test|spec))\\.[jt]sx?$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
  // 是否显示覆盖率报告
  collectCoverage: false,
};
